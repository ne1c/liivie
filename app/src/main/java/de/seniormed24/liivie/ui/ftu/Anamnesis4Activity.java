package de.seniormed24.liivie.ui.ftu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.CompoundButton;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.dialogs.MedicationDialog;
import de.seniormed24.liivie.ui.SplashActivity;
import de.seniormed24.liivie.ui.home.HomeActivity;
import de.seniormed24.liivie.views.ShadowContainer;

public class Anamnesis4Activity extends BaseFtuActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anamnesis4);
        AppCompatCheckBox cbConfirmation = findViewById(R.id.cbConfirmation);
        next2Confirm(false);
        cbConfirmation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                next2Confirm(b);
                ivNext.performClick();
            }
        });
        findViewById(R.id.btnMedSchedule1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSingleMedicine();
            }
        });
        findViewById(R.id.btnMedSchedule2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSingleMedicine();
            }
        });
        findViewById(R.id.btnMedScheduleAddMore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSingleMedicine();
            }
        });
    }


    protected void clickYes() {
        MedicationDialog medicationDialog = new MedicationDialog();
//        medicationDialog.setBackground(Blur.takeBackground(this));
        medicationDialog.show(getSupportFragmentManager(), "medicationDialog");
    }

    void openSingleMedicine() {
        startActivityForResult(new Intent(this, MedicineActivity.class), 123);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 123 && resultCode == RESULT_OK) {
            MedicineModel model = ((MedicineModel) data.getSerializableExtra("data"));
            Log.d("", "");

            int startIndex = 3;
            ViewGroup viewGroup = findViewById(R.id.container);
            ShadowContainer shadowContainer = new ShadowContainer(this);
            MarginLayoutParams params = new MarginLayoutParams(new LayoutParams(-1, -2));
            params.bottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, getResources().getDisplayMetrics());
            params.topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, getResources().getDisplayMetrics());
            params.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, getResources().getDisplayMetrics());
            params.rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, getResources().getDisplayMetrics());
            shadowContainer.setLayoutParams(params);
            Button button = new Button(this);
            button.setTextSize(16f);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(model.name);

            for (MedicineModel.Day day : model.days) {
                if (day != null) {
                    stringBuilder.append("\n");
                    stringBuilder.append(day.toString());
                }
            }

            stringBuilder.append(" exp. " + model.prescriptoionDate);
            button.setText(stringBuilder);
            button.setLayoutParams(new LayoutParams(-1, -2));

            shadowContainer.addView(button);
            shadowContainer.someMethod();
            viewGroup.addView(shadowContainer, viewGroup.getChildCount() - 4);

        }
    }

    @Override
    public void clickedNext() {
        getSharePref().setFtuFinished(true);
        startActivities(new Intent[]{new Intent(this, SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK),
                new Intent(this, HomeActivity.class)});
    }

    @Override
    void skipFtu() {

    }
}
