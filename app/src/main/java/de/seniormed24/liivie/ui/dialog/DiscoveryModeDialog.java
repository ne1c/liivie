package de.seniormed24.liivie.ui.dialog;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.dialogs.BaseDialog;

public class DiscoveryModeDialog extends BaseDialog {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_discovery_mode, container, false);
        view.findViewById(R.id.confirmDiscoveryDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BluetoothAdapter.getDefaultAdapter().enable();
                dismiss();
            }
        });
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }
}
