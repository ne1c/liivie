package de.seniormed24.liivie.ui.home.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.model.Vital;
import de.seniormed24.liivie.ui.BaseFragment;
import java.util.ArrayList;

public class VitalValFragment extends BaseFragment {

    public VitalValFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_vital_val, container, false);

        RecyclerView mRecyclerView = view.findViewById(R.id.rvVitalValFragment);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        ArrayList<Vital> mDataset = new ArrayList<>();
        mDataset.add(new Vital("activity index", 88, "mm"));
        mDataset.add(new Vital("health index", 96, "mm"));

        Pair<Integer, Integer> hr = sharePref.getHeartRate(); //65
        mDataset.add(new Vital(getString(R.string.heart_rate), hr.first == 0 ? 65 : (hr.second + hr.first) /2, "bpm"));
        mDataset.add(new Vital(getString(R.string.hrv), 65, "%"));

        Pair<Integer, Integer> bp = sharePref.getBloodPressure(); // 120 80
        mDataset.add(new Vital(getString(R.string.blood_pressure), bp.first == 0 ? 120 : bp.second, bp.first == 0 ? "80" : bp.first.toString()));

        Pair<Integer, Integer> bt = sharePref.getTemp(); // 36
        mDataset.add(new Vital(getString(R.string.body_temperature), bt.first == 0 ? 36 : (bt.second + bt.first) / 2, "C"));
        mDataset.add(new Vital(getString(R.string.dehydration), 56, "%"));

        RecyclerView.Adapter mAdapter = new VitalValAdapter(mDataset);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

}
