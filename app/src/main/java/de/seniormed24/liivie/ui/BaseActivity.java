package de.seniormed24.liivie.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.utils.LLog;
import de.seniormed24.liivie.utils.SharePref;

public abstract class BaseActivity extends AppCompatActivity {

    private SharePref sharePref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //LLog.d(this, "onCreate()");
        Crashlytics.log(Log.INFO, LLog.TAG, this.getClass().getSimpleName() +  ":onCreate()");
        sharePref = SharePref.i(this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    public SharePref getSharePref() {
        return sharePref;
    }
}
