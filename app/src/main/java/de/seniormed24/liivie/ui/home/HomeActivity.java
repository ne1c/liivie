package de.seniormed24.liivie.ui.home;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.dialogs.HowCanIHelpYouDialog;
import de.seniormed24.liivie.dialogs.MedicalDeviceDialog;
import de.seniormed24.liivie.dialogs.MinMaxDialog;
import de.seniormed24.liivie.dialogs.MinMaxDialog.Listener;
import de.seniormed24.liivie.dialogs.TiredDialog;
import de.seniormed24.liivie.ui.BaseActivity;
import de.seniormed24.liivie.ui.SplashActivity;
import de.seniormed24.liivie.ui.dialog.DiscoveryModeDialog;
import de.seniormed24.liivie.ui.ftu.BmiActivity;
import de.seniormed24.liivie.ui.home.fragment.HistoryFragment;
import de.seniormed24.liivie.ui.home.fragment.VitalValFragment;
import de.seniormed24.liivie.utils.Const;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HomeActivity extends BaseActivity {

    private DrawerLayout mDrawerLayout;
    boolean firstFragment = true;
    View btnBackHome;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            dateTextView.setText(new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US).format(new Date()));
            handler.postDelayed(this, 1000);
        }
    };

    private TextView dateTextView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        switch (menuItem.getItemId()) {
                            case R.id.nav_start:
                                startStartActivity();
                                break;
                            case R.id.nav_ftu:
                                startFtuActivity();
                                break;
                            case R.id.nav_blood_presaure:
                                dialogBloodPreasure();
                                break;
                            case R.id.nav_temperature:
                                dialogTemperature();
                                break;
                            case R.id.nav_heart_rate:
                                dialogHeartRate();
                                break;
                            case R.id.nav_settings:
                                dialogSettings();
                                break;
                            case R.id.nav_medical_device:
                                dialogMedicalDevices();
                                break;

                        }

                        return true;
                    }
                });

        findViewById(R.id.btnDiscoveryMainAct).setOnClickListener(onDiscoveryClickListener);
        findViewById(R.id.btnHowCanIHelp).setOnClickListener(onHelpClickListener);
        findViewById(R.id.batteryMainAct).setOnClickListener(onBatteryClickListener);
        btnBackHome = findViewById(R.id.ivPrevious);
        btnBackHome.setOnClickListener(onBackClickListener);
        changeFragment(new VitalValFragment());

        findViewById(R.id.ivNext).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new HistoryFragment());
            }
        });

        findViewById(R.id.btnPlus).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
                if (fragment instanceof HistoryFragment) {
                    ((HistoryFragment) fragment).zoomIn();
                }
            }
        });

        findViewById(R.id.btnMinus).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
                if (fragment instanceof HistoryFragment) {
                    ((HistoryFragment) fragment).zoomOut();
                }
            }
        });
//        findViewById(R.id.btnHeaderHome).setOnClickListener(onHeaderClickListener);

        dateTextView = findViewById(R.id.dateTextView);
        dateTextView.setTypeface(Typeface.createFromAsset(getAssets(), "font/nokia.ttf"));

        findViewById(R.id.cameraButton).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MediaStore.ACTION_IMAGE_CAPTURE));
            }
        });
    }

    public void changeFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, fragment);
            if (this.firstFragment) {
                this.firstFragment = false;
            } else {
                fragmentTransaction.addToBackStack(null);
            }
            try {
                fragmentTransaction.commit();
            } catch (Exception e) {
                fragmentTransaction.commitAllowingStateLoss();
            }
        }
    }

    View.OnClickListener onHelpClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            clickHowCanIHelp();
        }
    };

    View.OnClickListener onBackClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onBackPressed();
//            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
//                getSupportFragmentManager().popBackStack();
//                btnBackHome.setVisibility(View.VISIBLE);
//            } else {
//                onBackPressed();
//                btnBackHome.setVisibility(View.GONE);
//            }
        }
    };

    protected void clickHowCanIHelp() {
        HowCanIHelpYouDialog howCanIHelpYouDialog = new HowCanIHelpYouDialog();
        howCanIHelpYouDialog.show(getSupportFragmentManager(), "howCanIHelpYouDialog");
    }

    View.OnClickListener onDiscoveryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            clickDiscovery();
        }
    };

    protected void clickDiscovery() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            DiscoveryModeDialog discoveryModeDialog = new DiscoveryModeDialog();
            //discoveryModeDialog.setBackground(Blur.takeBackground(this));
            discoveryModeDialog.show(getSupportFragmentManager(), "howCanIHelpYouDialog");
        }

        BluetoothAdapter.getDefaultAdapter().enable();

//        startActivity(new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS));
    }

    View.OnClickListener onBatteryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            clickBattery();
        }
    };

    protected void clickBattery() {
        TiredDialog tiredDialog = new TiredDialog();
//        tiredDialog.setBackground(Blur.takeBackground(this));
        tiredDialog.show(getSupportFragmentManager(), "tiredDialog");
    }

    View.OnClickListener onHeaderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startStartActivity();
        }
    };

    void startStartActivity() {
        getSharePref().setFtuFinished(false);
//        onBackPressed(); // go to splash
        startActivity(new Intent(this, SplashActivity.class));
    }

    void startFtuActivity() {
        getSharePref().setFtuFinished(false);
        startActivity(new Intent(this, BmiActivity.class));
    }

    protected void onClickLogo() {
        try {
            Intent i = new Intent("android.intent.action.VIEW");
            i.setData(Uri.parse(Const.COMPANY_URL));
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.msg_no_browser, Toast.LENGTH_SHORT).show();
        }
    }

    void dialogBloodPreasure() {
        MinMaxDialog minMaxDialog = MinMaxDialog.newInstance(40, 190);
        minMaxDialog.setTitle(getString(R.string.blood_pressure));
        minMaxDialog.setListener(new Listener() {
            @Override
            public void set(int min, int max) {
                getSharePref().setBloodPressure(min, max);
            }
        });
        minMaxDialog.show(getSupportFragmentManager(), "dialogBloodPreasure");
    }

    void dialogTemperature() {
        MinMaxDialog minMaxDialog = MinMaxDialog.newInstance(35, 37);
        minMaxDialog.setTitle(getString(R.string.temp));
        minMaxDialog.setListener(new Listener() {
            @Override
            public void set(int min, int max) {
                getSharePref().setTemp(min, max);
            }
        });
        minMaxDialog.show(getSupportFragmentManager(), "dialogTemperature");
    }

    void dialogHeartRate() {
        MinMaxDialog minMaxDialog = MinMaxDialog.newInstance(40, 200);
        minMaxDialog.setTitle(getString(R.string.heart_rate));
        minMaxDialog.setListener(new Listener() {
            @Override
            public void set(int min, int max) {
                getSharePref().setHeartRate(min, max);
            }
        });
        minMaxDialog.show(getSupportFragmentManager(), "dialogHeartRate");
    }

    void dialogSettings() {
        // TODO: 06.03.2019
    }

    void dialogMedicalDevices() {
        MedicalDeviceDialog medicalDeviceDialog = new MedicalDeviceDialog();
        medicalDeviceDialog.show(getSupportFragmentManager(), "medicalDeviceDialog");
    }

    @Override
    protected void onStart() {
        super.onStart();

        runnable.run();
    }

    @Override
    protected void onStop() {
        handler.removeCallbacks(runnable);

        super.onStop();
    }
}
