package de.seniormed24.liivie.ui;

import android.content.Context;
import android.support.v4.app.Fragment;
import butterknife.Unbinder;
import de.seniormed24.liivie.utils.SharePref;

public class BaseFragment extends Fragment {
    protected Unbinder unbinder;
    protected SharePref sharePref;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        sharePref = SharePref.i(context);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.unbinder != null) {
            this.unbinder.unbind();
        }
    }
}
