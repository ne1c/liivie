package de.seniormed24.liivie.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.ui.ftu.BmiActivity;
import de.seniormed24.liivie.ui.home.HomeActivity;

import static android.view.animation.Animation.INFINITE;
import static android.view.animation.Animation.RESTART;

public class SplashActivity extends BaseActivity {
    final Handler handler = new Handler();
    final Runnable r = new Runnable() {
        public void run() {
            if (getSharePref().isFtuFinished()) {
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            } else {
                startActivity(new Intent(SplashActivity.this, BmiActivity.class));
            }
        }
    };
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.activity_splash);

    }

    @Override
    protected void onStart() {
        super.onStart();


        handler.postDelayed(r, 7000);

        AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
        alphaAnimation.setRepeatCount(INFINITE);
        alphaAnimation.setRepeatMode(RESTART);
        alphaAnimation.setDuration(2000);

        findViewById(R.id.button).startAnimation(alphaAnimation);
        findViewById(R.id.button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(r);
                r.run();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        handler.removeCallbacks(r);
    }
}
