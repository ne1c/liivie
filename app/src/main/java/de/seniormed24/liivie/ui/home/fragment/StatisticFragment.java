package de.seniormed24.liivie.ui.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.ui.BaseFragment;
import de.seniormed24.liivie.ui.home.HomeActivity;

public class StatisticFragment extends BaseFragment {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistic, container, false);
        view.findViewById(R.id.loHeartRate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openChart();
            }
        });
        return view;
    }

    void openChart() {
        if (getActivity() != null) {
            ((HomeActivity) getActivity()).changeFragment(new ChartFragment());
        }
    }

}
