package de.seniormed24.liivie.ui.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.dialogs.MedicalDeviceDialog;
import de.seniormed24.liivie.ui.BaseFragment;
import de.seniormed24.liivie.ui.home.HomeActivity;
import de.seniormed24.liivie.views.CircleProgressView;

public class DetailsHealthFragment extends BaseFragment {
    @BindView(R.id.progressHeartRate)
    CircleProgressView progressHeartRate;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_health, container, false);
        ButterKnife.bind((Object) this, view);
        return view;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick({R.id.containerHeartRate, R.id.containerBloodPressure, R.id.containerTemperature})
    protected void clickContainers() {
        MedicalDeviceDialog medicalDeviceDialog = new MedicalDeviceDialog();
//        medicalDeviceDialog.setBackground(Blur.takeBackground(getActivity()));
        medicalDeviceDialog.show(getChildFragmentManager(), "medicalDeviceDialog");
    }

    @OnClick({R.id.mainMenu})
    protected void clickMainMenu() {
        if (getActivity() != null) {
            ((HomeActivity) getActivity()).changeFragment(new MainFragment());
        }
    }

    @OnClick({R.id.containerSweat})
    protected void clickSweat() {
        if (getActivity() != null) {
            ((HomeActivity) getActivity()).changeFragment(new ManuallyMeasureFragment());
        }
    }
}
