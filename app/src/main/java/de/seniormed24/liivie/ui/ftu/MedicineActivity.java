package de.seniormed24.liivie.ui.ftu;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.vicmikhailau.maskededittext.MaskedEditText;
import de.seniormed24.liivie.R;

public class MedicineActivity extends BaseFtuActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine);
//        hideHeader();
        next2Confirm(true);

        final TextView textView = findViewById(R.id.etMedicineDrAddress);

        textView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final View dialogView = LayoutInflater.from(MedicineActivity.this).inflate(R.layout.dialog_doctor, null);

                final AlertDialog dialog = new AlertDialog.Builder(MedicineActivity.this)
                        .setView(dialogView)
                        .create();

                dialog.show();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialogView.findViewById(R.id.confirmDiscoveryDialog)
                        .setOnClickListener(new OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    String steet = ((EditText) dialog.getWindow().findViewById(R.id.etAddressStreet2)).getText().toString();
                                                    String zipCode = ((EditText) dialog.getWindow().findViewById(R.id.etAddressZip2)).getText().toString();

                                                    textView.setText(steet + " " + zipCode);
                                                    dialog.dismiss();
                                                }
                                            }
                        );

            }
        });


        int[] groups = new int[] {R.id.monLayout, R.id.tueLayout, R.id.wedLayout, R.id.thuLayout, R.id.friLayout,R.id.satLayout,R.id.sunLayout};

        for (int j = 0; j < groups.length; j++) {
            ViewGroup viewGroup = findViewById(groups[j]);

            if (viewGroup.getChildAt(2) instanceof MaskedEditText) {
                MaskedEditText childAt = (MaskedEditText) viewGroup.getChildAt(2);
                childAt.setFilters(new InputFilter[]{new InputFilterTime()});
            }
        }

        ((EditText) findViewById(R.id.etMedicineDiscountDate)).setFilters(new InputFilter[] {new InputFilterDate()});
    }

    @Override
    public void clickedNext() {
        MedicineModel model = new MedicineModel();

        model.days.add(collectData(R.id.monLayout, "Mon"));
        model.days.add(collectData(R.id.tueLayout, "Tue"));
        model.days.add(collectData(R.id.wedLayout, "Wed"));
        model.days.add(collectData(R.id.thuLayout, "Thu"));
        model.days.add(collectData(R.id.friLayout, "Fri"));
        model.days.add(collectData(R.id.satLayout, "Sat"));
        model.days.add(collectData(R.id.sunLayout, "Sun"));

        model.name = ((EditText) findViewById(R.id.etMedicineName)).getText().toString();
        model.prescriptoionDate = ((EditText) findViewById(R.id.etMedicineDiscountDate)).getText().toString();

        Intent intent = new Intent();
        intent.putExtra("data", model);

        setResult(RESULT_OK, intent);
        finish();
    }

    private MedicineModel.Day collectData(int resId, String dayName) {
        ViewGroup viewGroup = findViewById(resId);
        MedicineModel.Day day = new MedicineModel.Day(dayName);

        int index = 0;

        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            if (viewGroup.getChildAt(i) instanceof CheckBox) {
                if (!((CheckBox) viewGroup.getChildAt(i)).isChecked()) {
                    return null;
                }
            } else if (viewGroup.getChildAt(i) instanceof MaskedEditText && index == 0) {
                MaskedEditText childAt = (MaskedEditText) viewGroup.getChildAt(i);
                childAt.setFilters(new InputFilter[] {new InputFilterTime()});
                day.time = childAt.getText().toString();
                index++;
            } else if (viewGroup.getChildAt(i) instanceof MaskedEditText && index == 1) {
                day.quentity = ((MaskedEditText) viewGroup.getChildAt(i)).getText().toString();
            }

        }

        return day;
    }
}
