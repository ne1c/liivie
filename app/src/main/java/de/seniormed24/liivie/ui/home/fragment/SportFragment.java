package de.seniormed24.liivie.ui.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.ui.BaseFragment;
import de.seniormed24.liivie.ui.home.HomeActivity;

public class SportFragment extends BaseFragment {

    View layoutDistanceSportAct, layoutSpeedSportAct, layoutTimeSportAct, layoutAltitudeSportAct, layoutHrSportAct;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sport, container, false);
        layoutDistanceSportAct = view.findViewById(R.id.layoutDistanceSportAct);
        layoutSpeedSportAct = view.findViewById(R.id.layoutSpeedSportAct);
        layoutTimeSportAct = view.findViewById(R.id.layoutTimeSportAct);
        layoutAltitudeSportAct = view.findViewById(R.id.layoutAltitudeSportAct);
        layoutHrSportAct = view.findViewById(R.id.layoutHrSportAct);
        setSwitch(view, R.id.btnDistanceSportAct);
        setSwitch(view, R.id.btnSpeedSportAct);
        setSwitch(view, R.id.btnChronometrSportAct);
        setSwitch(view, R.id.btnAltitudeSportAct);
        setSwitch(view, R.id.btnHeartrateSportAct);
        return view;
    }

    void setSwitch(View v, int id) {
        View btn = v.findViewById(id);
        btn.setActivated(true);
        btn.setOnClickListener(onItemClickListener);
    }

    View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnDistanceSportAct:
                    btnSwitch(view, layoutDistanceSportAct);
                    break;
                case R.id.btnSpeedSportAct:
                    btnSwitch(view, layoutSpeedSportAct);
                    break;
                case R.id.btnChronometrSportAct:
                    btnSwitch(view, layoutTimeSportAct);
                    break;
                case R.id.btnAltitudeSportAct:
                    btnSwitch(view, layoutAltitudeSportAct);
                    break;
                case R.id.btnHeartrateSportAct:
                    btnSwitch(view, layoutHrSportAct);
                    break;
            }
        }
    };

    void btnSwitch(View vButton, View vControled) {
        if (vControled.getVisibility() == View.VISIBLE) {
            vControled.setVisibility(View.GONE);
            vButton.setActivated(false);
        } else {
            vControled.setVisibility(View.VISIBLE);
            vButton.setActivated(true);
        }
    }

    protected void clickMainMenu() {
        if (getActivity() != null) {
            ((HomeActivity) getActivity()).changeFragment(new MainFragment());
        }
    }


}
