package de.seniormed24.liivie.ui.ftu;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class InputWatcher implements TextWatcher {
    private EditText mEditText;
    private int min;
    private int max;

    public InputWatcher(EditText editText, int min, int max) {
        mEditText = editText;
        this.min = min;
        this.max = max;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.toString().isEmpty()) return;

        try {
            int value = Integer.parseInt(s.toString());

            if (value < min || value > max) {
                mEditText.setText("");
            }
        } catch (Exception e) {

        }
    }
}
