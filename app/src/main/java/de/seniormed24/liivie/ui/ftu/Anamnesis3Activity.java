package de.seniormed24.liivie.ui.ftu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.dialogs.InputDialog;

public class Anamnesis3Activity extends BaseFtuActivity {

    RadioButton rbNoQ2, rbFoodSupplements;

    public static void startActivity(Context context, String from) {
        Intent intent = new Intent(context, Anamnesis3Activity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anamnesis3);

        CheckBox cbOtherDrinks = findViewById(R.id.cbOtherDrinks);
        cbOtherDrinks.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) clickOtherDrinks();
            }
        });

        rbFoodSupplements = findViewById(R.id.rbFoodSupplements);
        rbFoodSupplements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickFoodSupplements(true);
            }
        });

        rbNoQ2 = findViewById(R.id.rbNoQ2);
        rbNoQ2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickFoodSupplements(false);
            }
        });

        CheckBox cbOtherTherapies = findViewById(R.id.cbOtherTherapies);
        cbOtherTherapies.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) clickOtherTherapies();
            }
        });

    }

    protected void clickOtherDrinks() {
        InputDialog inputDialog = new InputDialog();
//        inputDialog.setBackground(Blur.takeBackground(this));
        inputDialog.setTitle(R.string.following_drinks);
        inputDialog.show(getSupportFragmentManager(), "input_dialog");
    }

    protected void clickOtherTherapies() {
        InputDialog inputDialog = new InputDialog();
//        inputDialog.setBackground(Blur.takeBackground(this));
        inputDialog.setTitle(R.string.other_therapies);
        inputDialog.show(getSupportFragmentManager(), "input_dialog");
    }

    protected void clickFoodSupplements(boolean foodSupplement) {
        rbNoQ2.setChecked(!foodSupplement);
        rbFoodSupplements.setChecked(foodSupplement);
        if (foodSupplement) {
            InputDialog inputDialog = new InputDialog();
//        inputDialog.setBackground(Blur.takeBackground(this));
            inputDialog.setTitle(R.string.which_food_supplements);
            inputDialog.show(getSupportFragmentManager(), "input_dialog");
        }
    }


    @Override
    public void clickedNext() {
        startActivity(new Intent(this, Anamnesis4Activity.class));
    }
}
