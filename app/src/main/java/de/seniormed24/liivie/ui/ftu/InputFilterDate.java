package de.seniormed24.liivie.ui.ftu;

import android.text.InputFilter;
import android.text.Spanned;

public class InputFilterDate implements InputFilter {

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            String str = dest.toString() + source.toString();
            String[] date = str.split("\\.");

            if (date.length == 1) {
                if ((Integer.parseInt(date[0]) >= 0 && Integer.parseInt(date[0]) <= 31) || date[0].length() == 3) {
                    return null;
                }
            } else if (date.length == 2) {
                if ((Integer.parseInt(date[1]) >= 0 && Integer.parseInt(date[1]) <= 12) || date[1].length() == 3) {
                    return null;
                }
            } else if (date.length == 3) {
                if (Integer.parseInt(date[2]) < 2019) {
                    return null;
                }
            }
                return "";
        } catch (NumberFormatException nfe) {
        }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}