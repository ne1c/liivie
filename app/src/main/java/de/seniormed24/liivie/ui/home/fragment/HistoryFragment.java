package de.seniormed24.liivie.ui.home.fragment;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import de.seniormed24.liivie.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class HistoryFragment extends Fragment {


    public HistoryFragment() {
        // Required empty public constructor
    }

    LineChart first;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            dateTextView.setText(new SimpleDateFormat("HH:mm:ss", Locale.US).format(new Date()));
            handler.postDelayed(this, 1000);
        }
    };

    private TextView dateTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dateTextView = view.findViewById(R.id.dateTextView);
        final TextView calendarTextView = view.findViewById(R.id.textView2);

        final TextView today = view.findViewById(R.id.todayTextView);
        final TextView yest = view.findViewById(R.id.yestTextView);
        final TextView week = view.findViewById(R.id.weekTextView);
        final TextView month = view.findViewById(R.id.monthTextView);
        final TextView individual = view.findViewById(R.id.individualTextView);

        today.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                today.setTypeface(Typeface.DEFAULT_BOLD);
                yest.setTypeface(Typeface.DEFAULT);
                week.setTypeface(Typeface.DEFAULT);
                month.setTypeface(Typeface.DEFAULT);
                individual.setTypeface(Typeface.DEFAULT);

                calendarTextView.setVisibility(View.INVISIBLE);
            }
        });

        yest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                today.setTypeface(Typeface.DEFAULT);
                yest.setTypeface(Typeface.DEFAULT_BOLD);
                week.setTypeface(Typeface.DEFAULT);
                month.setTypeface(Typeface.DEFAULT);
                individual.setTypeface(Typeface.DEFAULT);

                calendarTextView.setVisibility(View.INVISIBLE);
            }
        });

        week.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                today.setTypeface(Typeface.DEFAULT);
                yest.setTypeface(Typeface.DEFAULT);
                week.setTypeface(Typeface.DEFAULT_BOLD);
                month.setTypeface(Typeface.DEFAULT);
                individual.setTypeface(Typeface.DEFAULT);

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                calendarTextView.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                calendarTextView.setVisibility(View.VISIBLE);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        month.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                today.setTypeface(Typeface.DEFAULT);
                yest.setTypeface(Typeface.DEFAULT);
                week.setTypeface(Typeface.DEFAULT);
                month.setTypeface(Typeface.DEFAULT_BOLD);
                individual.setTypeface(Typeface.DEFAULT);

                calendarTextView.setVisibility(View.INVISIBLE);
            }
        });

        individual.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                today.setTypeface(Typeface.DEFAULT);
                yest.setTypeface(Typeface.DEFAULT);
                week.setTypeface(Typeface.DEFAULT);
                month.setTypeface(Typeface.DEFAULT);
                individual.setTypeface(Typeface.DEFAULT_BOLD);

                calendarTextView.setVisibility(View.INVISIBLE);
            }
        });

        runnable.run();

        first = view.findViewById(R.id.chart);
        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(1, 50));
        values.add(new Entry(2, -50));
        values.add(new Entry(3, 0));
        values.add(new Entry(4, 0));
        values.add(new Entry(5, 50));
        values.add(new Entry(6, -50));
        values.add(new Entry(7, 0));
        values.add(new Entry(8, 0));
        values.add(new Entry(9, 50));
        values.add(new Entry(10, -50));
        values.add(new Entry(11, 0));
        values.add(new Entry(12, 0));
        values.add(new Entry(13, 50));
        values.add(new Entry(14, -50));
        values.add(new Entry(15, 50));
        values.add(new Entry(16, -50));
        values.add(new Entry(17, 0));
        values.add(new Entry(18, 0));

        setDataInChart(first, values);

        view.findViewById(R.id.zoomIn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                first.zoomIn();
            }
        });

        view.findViewById(R.id.zoomOut).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                first.zoomOut();
            }
        });
    }

    public void zoomIn() {
        first.zoomIn();
    }

    public void zoomOut() {
        first.zoomOut();
    }

    @Override
    public void onDestroyView() {
        handler.removeCallbacks(runnable);

        super.onDestroyView();
    }

    private void setDataInChart(LineChart mChart, ArrayList<Entry> values) {
        LineDataSet set1;
        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, "Sample Data");
            set1.setDrawIcons(false);
            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.DKGRAY);
            set1.setCircleColor(Color.DKGRAY);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(false);
            set1.setDrawCircles(false);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);
//            if (Utils.getSDKInt() >= 18) {
//                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_blue);
//                set1.setFillDrawable(drawable);
//            } else {
                set1.setFillColor(Color.DKGRAY);
//            }
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            LineData data = new LineData(dataSets);
            mChart.setData(data);
        }
//        LineDataSet set1;
//        if (mChart.getData() != null &&
//                mChart.getData().getDataSetCount() > 0) {
//            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
//            set1.setValues(values);
//            mChart.getData().notifyDataChanged();
//            mChart.notifyDataSetChanged();
//        } else {
//            set1 = new LineDataSet(values, "Sample Data");
//            set1.setDrawIcons(false);
//            set1.enableDashedLine(10f, 5f, 0f);
//            set1.enableDashedHighlightLine(10f, 5f, 0f);
//            set1.setColor(Color.DKGRAY);
//            set1.setCircleColor(Color.DKGRAY);
//            set1.setLineWidth(1f);
//            set1.setCircleRadius(3f);
//            set1.setDrawCircleHole(false);
//            set1.setValueTextSize(9f);
//            set1.setDrawFilled(true);
//            set1.setFormLineWidth(1f);
//            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
//            set1.setFormSize(15.f);
////            if (Utils.getSDKInt() >= 18) {
////                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_blue);
////                set1.setFillDrawable(drawable);
////            } else {
//                set1.setFillColor(Color.DKGRAY);
////            }
//            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
//            dataSets.add(set1);
//            LineData data = new LineData(dataSets);
//            mChart.setData(data);
//        }
    }
}
