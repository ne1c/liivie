package de.seniormed24.liivie.ui.ftu;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import de.seniormed24.liivie.App;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.dialogs.InputDialog;

public class Anamnesis2Activity extends BaseFtuActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anamnesis2);

        View cbPregnant = findViewById(R.id.cbPregnant);
        View cbBreastfeeding = findViewById(R.id.cbBreastfeeding);

        if (!App.getInstance().getLocalData().female) {
            cbPregnant.setVisibility(View.GONE);
            cbBreastfeeding.setVisibility(View.GONE);
        } else {
            cbPregnant.setVisibility(View.VISIBLE);
            cbBreastfeeding.setVisibility(View.VISIBLE);
        }


        CheckBox cbSupplementOther = findViewById(R.id.cbDiagnosisOther);
        cbSupplementOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    clickOtherSupplement();
            }
        });


        CheckBox cbAllergies = findViewById(R.id.cbAllergies);
        cbAllergies.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    clickAllergies();
            }
        });

        ((EditText) findViewById(R.id.hr_max)).setFilters(new InputFilter[]{new InputFilterMinMax(1, 200)});
        ((EditText) findViewById(R.id.hr_min)).setFilters(new InputFilter[]{new InputFilterMinMax(1, 200)});
        ((EditText) findViewById(R.id.bp_max)).setFilters(new InputFilter[]{new InputFilterMinMax(1, 190)});
        ((EditText) findViewById(R.id.bp_min)).setFilters(new InputFilter[]{new InputFilterMinMax(1, 190)});
    }

    protected void clickOtherSupplement() {
        InputDialog inputDialog = new InputDialog();
//        inputDialog.setBackground(Blur.takeBackground(this));
        inputDialog.setTitle(R.string.following_other_diagnosis);
        inputDialog.show(getSupportFragmentManager(), "input_dialog");
    }

    protected void clickAllergies() {
        InputDialog inputDialog = new InputDialog();
//        inputDialog.setBackground(Blur.takeBackground(this));
        inputDialog.setTitle(R.string.following_allergies);
        inputDialog.show(getSupportFragmentManager(), "input_dialog");
    }


    @Override
    public void clickedNext() {
        startActivity(new Intent(this, Anamnesis3Activity.class));
    }
}


