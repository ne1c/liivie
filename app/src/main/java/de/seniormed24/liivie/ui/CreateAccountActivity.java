package de.seniormed24.liivie.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.ui.ftu.BmiActivity;
import de.seniormed24.liivie.ui.home.HomeActivity;
import de.seniormed24.liivie.utils.Const;
import de.seniormed24.liivie.views.CustomPasswordTransformationMethod;

@Deprecated
public class CreateAccountActivity extends BaseActivity {
    EditText password;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_create_account);
        this.password.setTransformationMethod(new CustomPasswordTransformationMethod());
    }

    protected void onClickSkip() {
        startActivity(new Intent(this, HomeActivity.class));
    }

    protected void onClickCreate() {
        BmiActivity.startActivity(this, Const.FROM_CREATE_ACCOUNT);
    }
}
