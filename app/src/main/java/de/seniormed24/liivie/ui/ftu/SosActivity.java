package de.seniormed24.liivie.ui.ftu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import de.seniormed24.liivie.R;

public class SosActivity extends BaseFtuActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sos);
//        hideHeader();
        next2Confirm(true);

        View otherPersons = findViewById(R.id.otherPersons);
        otherPersons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMorePersons();
            }
        });
    }

    @Override
    public void clickedNext() {
        startActivity(new Intent(this, Anamnesis2Activity.class));
    }

    void startMorePersons() {
        startActivity(new Intent(this, SosActivity.class));
    }
}
