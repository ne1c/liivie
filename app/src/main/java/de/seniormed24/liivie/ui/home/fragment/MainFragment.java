package de.seniormed24.liivie.ui.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.ui.BaseFragment;
import de.seniormed24.liivie.ui.ftu.BmiActivity;
import de.seniormed24.liivie.ui.home.HomeActivity;
import de.seniormed24.liivie.utils.Const;

@Deprecated
public class MainFragment extends BaseFragment {

    View btnStatisticsMainFragment;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        btnStatisticsMainFragment = view.findViewById(R.id.btnStatisticsMainFragment);
        btnStatisticsMainFragment.setOnClickListener(onClickListener);
        ButterKnife.bind((Object) this, view);
        return view;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnStatisticsMainFragment:
                    if (getActivity() != null) {
                        ((HomeActivity) getActivity()).changeFragment(new StatisticFragment());
                    }
                    break;

            }

        }
    };

    @OnClick({R.id.btnSportMainFrag})
    protected void clickSport() {
        if (getActivity() != null) {
            ((HomeActivity) getActivity()).changeFragment(new SportFragment());
        }
    }

    @OnClick({R.id.btnVitalMainFrag})
    protected void clickMenu() {
        ((HomeActivity) MainFragment.this.getActivity()).changeFragment(new VitalValFragment());
        /*
        MenuDialog menuDialog = new MenuDialog();
        //menuDialog.setBackground(Blur.takeBackground(getActivity()));
        menuDialog.setListener(new IClickMenuListener() {
            public void onClick(int menuId) {
                if (menuId == 1) {
                    if (MainFragment.this.getActivity() != null) {
                        ((HomeActivity) MainFragment.this.getActivity()).changeFragment(new DetailsHealthFragment());
                    }
                }
            }
        });
        menuDialog.show(getChildFragmentManager(), "input_dialog");
        */
    }

    @OnClick({R.id.cloud})
    protected void clickCloud() {
    }

    @OnClick({R.id.settings})
    protected void clickSettings() {
        BmiActivity.startActivity(getActivity(), Const.FROM_SETTING);
    }

    @OnClick({R.id.heartRate, R.id.temperature, R.id.bloodPressure, R.id.hrv, R.id.sweet})
    protected void clickMenus() {
    }
}
