package de.seniormed24.liivie.ui.ftu;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.ui.BaseActivity;
import de.seniormed24.liivie.ui.home.HomeActivity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public abstract class BaseFtuActivity extends BaseActivity {

    ImageView ivNext;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            dateTextView.setText(new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US).format(new Date()));
            handler.postDelayed(this, 1000);
        }
    };

    private TextView dateTextView;
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base);
        FrameLayout frameContent = findViewById(R.id.layoutBaseFrame);
        frameContent.removeAllViews();
        LayoutInflater.from(this).inflate(layoutResID, frameContent);

        View btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ivNext = findViewById(R.id.ivNext);
        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickedNext();
            }
        });

        findViewById(R.id.btnSkipFtu).setOnClickListener(onClickListener);
        findViewById(R.id.ivPrevious).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.btnHeaderHome).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                skipFtu();
            }
        });

        dateTextView = findViewById(R.id.dateTextView);
        dateTextView.setTypeface(Typeface.createFromAsset(getAssets(), "font/nokia.ttf"));
    }

    @Override
    protected void onStart() {
        super.onStart();

        runnable.run();
    }

    @Override
    protected void onStop() {
        handler.removeCallbacks(runnable);

        super.onStop();
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnSkipFtu:
                    skipFtu();
                    break;
            }
        }
    };

    /*
     * Skip without saving data
     */
    void skipFtu() {
        startActivity(new Intent(this, HomeActivity.class));
    }

    abstract public void clickedNext();

//    public void hideHeader() {
//        View ivHeaderBaseActivity = findViewById(R.id.ivHeaderBaseActivity);
//        ivHeaderBaseActivity.setVisibility(View.GONE);
//    }

    public void next2Confirm(boolean enabled) {
        if (!enabled) {
            ivNext.setVisibility(View.GONE);
        } else {
            ivNext.setVisibility(View.VISIBLE);
        }
//        ivNext.setImageDrawable(getDrawable(R.drawable.selector_back));
//        ivNext.setRotation(0);

        ivNext.setEnabled(enabled);
    }

}
