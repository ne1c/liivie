package de.seniormed24.liivie.ui.home.fragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.model.Vital;

class VitalValAdapter extends android.support.v7.widget.RecyclerView.Adapter<VitalValAdapter.MyViewHolder> {

    private ArrayList<Vital> mDataset;

    VitalValAdapter(ArrayList<Vital> dataset) {
        mDataset = dataset;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_vital_val, parent, false);

        // Return a new holder instance
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.tvTitleItemVital.setText(mDataset.get(i).getTitle());
        myViewHolder.tvValueCircle.setText(""+mDataset.get(i).getValue());
        myViewHolder.tvUnitCircle.setText(mDataset.get(i).getUnit());
        myViewHolder.pbValueCircle.setProgress((int) mDataset.get(i).getValue());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitleItemVital, tvValueCircle, tvUnitCircle;
        public ProgressBar pbValueCircle;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitleItemVital = itemView.findViewById(R.id.tvTitleItemVital);
            tvValueCircle = itemView.findViewById(R.id.tvValueCircle);
            tvUnitCircle = itemView.findViewById(R.id.tvUnitCircle);
            pbValueCircle = itemView.findViewById(R.id.pbValueCircle);
        }

    }
}
