package de.seniormed24.liivie.ui.ftu;

import android.text.InputFilter;
import android.text.Spanned;

public class InputFilterTime implements InputFilter {

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            String str = dest.toString() + source.toString();
            String[] time = str.split(":");

            if (str.length() == 3 && (str.contains(":") || !str.contains(":"))) return null;

            if (time.length == 1) {
                if (Integer.parseInt(time[0]) > 23) {
                    return "";
                }
            } else if (time.length == 2) {
                if (Integer.parseInt(time[0]) > 23) {
                    return "";
                }
                if (Integer.parseInt(time[1]) > 59) {
                    return "";
                }
            }
//            if (isInRange(min, max, input))
                return null;
        } catch (NumberFormatException nfe) {
        }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}