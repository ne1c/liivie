package de.seniormed24.liivie.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.ui.ftu.BmiActivity;
import de.seniormed24.liivie.ui.home.HomeActivity;

public class StartActivity extends BaseActivity {

    TextView txt8Senses, txtDoc, txtSport, txtTour;

    int lastOneSelected = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSharePref().isFtuFinished()) {
            startActivity(new Intent(this, HomeActivity.class));
            this.finish();
        }

        setContentView(R.layout.activity_start);

        txt8Senses = findViewById(R.id.txt8Senses);
        txtDoc = findViewById(R.id.txtDoc);
        txtSport = findViewById(R.id.txtSport);
        txtTour = findViewById(R.id.txtTour);

        View btnSenses = findViewById(R.id.btnSenses);
        View btnDoc = findViewById(R.id.btnDoc);
        View btnSport = findViewById(R.id.btnSport);
        View btnTour = findViewById(R.id.btnMessinger);

        btnSenses.setOnClickListener(onClickListener);
        btnDoc.setOnClickListener(onClickListener);
        btnSport.setOnClickListener(onClickListener);
        btnTour.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnSenses:
                    onClickSense();
                    break;
                case R.id.btnDoc:
                    onClickDoc();
                    break;
                case R.id.btnSport:
                    onClickSport();
                    break;
                case R.id.btnMessinger:
                    onClickMessinger();
                    break;
            }
        }
    };

    protected void onClickSport() {
        if (lastOneSelected != R.id.txtSport)
            show(R.id.txtSport);
        else
            Toast.makeText(this, "Start SPORT", Toast.LENGTH_SHORT).show();
    }

    protected void onClickMessinger() {
        if (lastOneSelected != R.id.txtTour)
            show(R.id.txtTour);
        else
            Toast.makeText(this, "Start Messenger", Toast.LENGTH_SHORT).show();
    }

    protected void onClickDoc() {
        if (lastOneSelected != R.id.txtDoc)
            show(R.id.txtDoc);
        else
            Toast.makeText(this, "Start DOC", Toast.LENGTH_SHORT).show();
    }

    protected void onClickSense() {
        if (lastOneSelected == R.id.txt8Senses)
            startActivity(new Intent(this, BmiActivity.class));
        else
            show(R.id.txt8Senses);
    }

    void show(int txtSelected) {

        lastOneSelected = txtSelected;

        if (txtSelected == R.id.txt8Senses)
            txt8Senses.setVisibility(View.VISIBLE);
        else
            txt8Senses.setVisibility(View.GONE);

        if (txtSelected == R.id.txtDoc)
            txtDoc.setVisibility(View.VISIBLE);
        else
            txtDoc.setVisibility(View.GONE);

        if (txtSelected == R.id.txtSport)
            txtSport.setVisibility(View.VISIBLE);
        else
            txtSport.setVisibility(View.GONE);

        if (txtSelected == R.id.txtTour)
            txtTour.setVisibility(View.VISIBLE);
        else
            txtTour.setVisibility(View.GONE);
    }
}
