package de.seniormed24.liivie.ui.ftu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import de.seniormed24.liivie.R;

public class FamilyDrActivity extends BaseFtuActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_family_dr);
//        hideHeader();

        View otherDr = findViewById(R.id.otherDr);
        otherDr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startOtherDr();
            }
        });
    }

    @Override
    public void clickedNext() {
        startActivity(new Intent(this, SosActivity.class));
    }

    void startOtherDr() {
        startActivity(new Intent(this, FamilyDrActivity.class));
    }
}
