package de.seniormed24.liivie.ui.ftu;

import android.content.Intent;
import android.os.Bundle;

import de.seniormed24.liivie.R;

public class InsurenceActivity extends BaseFtuActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurence);
//        hideHeader();
    }

    @Override
    public void clickedNext() {
        startActivity(new Intent(this, FamilyDrActivity.class));
    }
}
