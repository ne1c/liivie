package de.seniormed24.liivie.ui.home.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.ui.BaseFragment;

public class ChartFragment extends BaseFragment {
    @BindView(R.id.chart)
    protected LineChart mChart;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chart, container, false);
        ButterKnife.bind((Object) this, view);
        return view;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
        this.mChart.getDescription().setEnabled(false);
        this.mChart.getLegend().setEnabled(false);
        XAxis xAxis = this.mChart.getXAxis();
        xAxis.setPosition(XAxisPosition.BOTTOM);
        xAxis.setTextSize(10.0f);
        xAxis.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        xAxis.setTextColor(-16777216);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setCenterAxisLabels(true);
        xAxis.setGranularity(1.0f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            private SimpleDateFormat mFormat = new SimpleDateFormat("HH aa", Locale.ENGLISH);

            public String getFormattedValue(float value, AxisBase axis) {
                return this.mFormat.format(new Date(TimeUnit.HOURS.toMillis((long) value)));
            }
        });
        YAxis leftAxis = this.mChart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setTypeface(Typeface.create("sans-serif-black", Typeface.NORMAL));
        leftAxis.setDrawAxisLine(true);
        leftAxis.setAxisLineWidth(2.0f);
        leftAxis.setAxisLineColor(-16777216);
        leftAxis.setTextColor(-16777216);
        leftAxis.setDrawGridLines(false);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setAxisMinimum(50.0f);
        leftAxis.setAxisMaximum(100.0f);
        leftAxis.setYOffset(0.0f);
        this.mChart.getAxisRight().setEnabled(false);
    }

    @OnClick({R.id.back})
    protected void clickSettings() {
        getActivity().onBackPressed();
    }

    private void setData() {
        long now = TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis());
        ArrayList<Entry> values = new ArrayList();
        float to = (float) (((long) 10) + now);
        for (float x = (float) now; x < to; x += 1.0f) {
            values.add(new Entry(x, ((float) (Math.random() * 30.0d)) + 60.0f));
        }
        LineDataSet set1 = new LineDataSet(values, "");
        set1.setAxisDependency(AxisDependency.LEFT);
        set1.setColor(-16777216);
        set1.setLineWidth(1.5f);
        set1.setDrawCircles(true);
        set1.setCircleColor(-16777216);
        set1.setCircleRadius(3.0f);
        set1.setDrawValues(false);
        set1.setFillAlpha(65);
        set1.setDrawCircleHole(false);
        LineData data = new LineData(set1);
        data.setValueTextColor(-1);
        data.setValueTextSize(9.0f);
        this.mChart.setData(data);
    }
}
