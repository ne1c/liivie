package de.seniormed24.liivie.ui.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.dialogs.MeasuredValuesDialog;
import de.seniormed24.liivie.ui.BaseFragment;

public class ManuallyMeasureFragment extends BaseFragment {
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manually_measure, container, false);
        View ivMeasureAnim = view.findViewById(R.id.ivMeasureAnim);
        ivMeasureAnim.setOnClickListener(onClickListener);
        view.findViewById(R.id.back).setOnClickListener(onClickListener);
        return view;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivMeasureAnim:
                    showMeasuredDialog();
                    break;
                case R.id.back:
                    clickBack();
                    break;
            }
        }
    };

    protected void clickBack() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    void showMeasuredDialog() {
        MeasuredValuesDialog howCanIHelpYouDialog = new MeasuredValuesDialog();
        howCanIHelpYouDialog.show(getActivity().getSupportFragmentManager(), "MeasuredValuesDialog");
    }
}
