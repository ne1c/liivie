package de.seniormed24.liivie.ui.ftu;

import java.io.Serializable;
import java.util.ArrayList;

public class MedicineModel implements Serializable {
    public String prescriptoionDate = "";
    public String name = "";

    public ArrayList<MedicineModel.Day> days = new ArrayList<>();

    static class Day implements Serializable  {
        public final String day;

        public String time = "00:00";
        public String quentity = "1";

        public Day(String day) {
            this.day = day;
        }

        @Override
        public String toString() {
            return day + ". " + time + " " + quentity + "x";
        }
    }
}
