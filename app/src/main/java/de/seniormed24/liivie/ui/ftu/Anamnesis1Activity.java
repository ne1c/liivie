package de.seniormed24.liivie.ui.ftu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import de.seniormed24.liivie.R;
import de.seniormed24.liivie.utils.Const;
import de.seniormed24.liivie.utils.SharedPreferenceUtils;

public class Anamnesis1Activity extends BaseFtuActivity {

    EditText etAddress1;
    EditText etAddress2;
    EditText etPhone1;
    EditText etPhone2;

    private SharedPreferenceUtils sp;

    public static void startActivity(Context context, String from) {
        Intent intent = new Intent(context, Anamnesis1Activity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anamnesis1);

        etAddress1 =  findViewById(R.id.etAddress1);
        etAddress2 =  findViewById(R.id.etAddress2);
        etPhone1 =  findViewById(R.id.etPhone1);
        etPhone2 =  findViewById(R.id.etPhone2);

        this.sp = SharedPreferenceUtils.getInstance(this);
        String from = getIntent().getStringExtra("from");

        setSP();

        View txInsurence = findViewById(R.id.txInsurence);
        txInsurence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startInsurenceActivity();
            }
        });
    }

    void startInsurenceActivity() {
        startActivity(new Intent(this, InsurenceActivity.class));
    }

    private void saveSP() {
        this.sp.setValue(Const.ADDRESS1, this.etAddress1.getText().toString().trim());
        this.sp.setValue(Const.ADDRESS2, this.etAddress2.getText().toString().trim());
        this.sp.setValue(Const.PHONE1, this.etPhone1.getText().toString().trim());
        this.sp.setValue(Const.PHONE2, this.etPhone2.getText().toString().trim());
    }

    private void setSP() {
        this.etAddress1.setText(this.sp.getStringValue(Const.ADDRESS1, ""));
        this.etAddress2.setText(this.sp.getStringValue(Const.ADDRESS2, ""));
        this.etPhone1.setText(this.sp.getStringValue(Const.PHONE1, ""));
        this.etPhone2.setText(this.sp.getStringValue(Const.PHONE2, ""));
    }

    @Override
    public void clickedNext() {
        startActivity(new Intent(this, Anamnesis2Activity.class));
    }
}
