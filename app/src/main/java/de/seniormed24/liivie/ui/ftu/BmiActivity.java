package de.seniormed24.liivie.ui.ftu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.webianks.library.scroll_choice.ScrollChoice;
import com.webianks.library.scroll_choice.ScrollChoice.OnItemSelectedListener;
import de.seniormed24.liivie.App;
import de.seniormed24.liivie.R;
import java.util.ArrayList;
import java.util.List;

public class BmiActivity extends BaseFtuActivity {

    private View ivMan;
    private View ivWoman;

    public static void startActivity(Context context, String from) {
        Intent intent = new Intent(context, BmiActivity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        ScrollChoice height = findViewById(R.id.scroll_choice);
        List<String> data = new ArrayList();
        for (int i = 100; i <= Callback.DEFAULT_DRAG_ANIMATION_DURATION; i++) {
            data.add(String.valueOf(i));
        }

        height.addItems(data, 1);
        height.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(ScrollChoice scrollChoice, int position, String name) {
//                BmiActivity.this.position1 = position;
            }
        });
        ScrollChoice weight = findViewById(R.id.scroll_choice_two);
        List<String> data2 = new ArrayList();
        for (int i2 = 50; i2 <= 150; i2++) {
            data2.add(String.valueOf(i2));
        }
        weight.addItems(data2, 1);
        weight.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(ScrollChoice scrollChoice, int position, String name) {
//                BmiActivity.this.position2 = position;
            }
        });
        ivMan = findViewById(R.id.ivMan);
        ivMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMan();
            }
        });

        ivWoman = findViewById(R.id.ivWoman);
        ivWoman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setWomen();
            }
        });

        EditText day = (EditText) findViewById(R.id.etDay);
        day.setFilters(new InputFilter[]{new InputFilterMinMax(1, 31)});
        day.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 2) {
                    findViewById(R.id.etMonth).requestFocus();
                }
            }
        });

        EditText month = (EditText) findViewById(R.id.etMonth);
        month.setFilters(new InputFilter[]{new InputFilterMinMax(1, 12)});

        month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 2) {
                    findViewById(R.id.etYear).requestFocus();
                }
            }
        });

        final EditText year = (EditText) findViewById(R.id.etYear);
        year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() > 4 || (s.length() == 4 && Integer.parseInt(s.toString()) < 1900 || Integer.parseInt(s.toString()) > 2019)) {
                        year.setText("");
                    }
                } catch (Exception e) {

                }
            }
        });
//        year.setFilters(new InputFilter[]{new InputFilterMinMax(0, Calendar.getInstance().get(Calendar.YEAR))});

    }

    @Override
    public void clickedNext() {
        startActivity(new Intent(this, Anamnesis1Activity.class));
    }

    void setWomen() {
        ivMan.setActivated(false);
        ivWoman.setActivated(true);
        App.getInstance().getLocalData().female = true;
    }

    void setMan() {
        ivWoman.setActivated(false);
        ivMan.setActivated(true);
        App.getInstance().getLocalData().female = false;
    }

}
