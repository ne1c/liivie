package de.seniormed24.liivie.dialogs;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.seniormed24.liivie.R;

public class InputDialog extends BaseDialog {
    @BindView(R.id.fragmentContainer)
    View containerMain;
    @BindView(R.id.etOther)
    EditText etOther;
    int strTitle;
    @BindView(R.id.title)
    TextView title;

    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_input, container, false);
        ButterKnife.bind(this, view);
        this.containerMain.setBackground(new BitmapDrawable(getResources(), this.background));
        this.title.setText(this.strTitle);
        this.etOther.setImeOptions(6);
        this.etOther.setRawInputType(1);
        return view;
    }

    @OnClick({R.id.ok})
    protected void clickOk() {
        dismiss();
    }

    public void setTitle(int title) {
        this.strTitle = title;
    }
}
