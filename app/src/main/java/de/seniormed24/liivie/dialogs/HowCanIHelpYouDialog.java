package de.seniormed24.liivie.dialogs;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.seniormed24.liivie.R;

public class HowCanIHelpYouDialog extends BaseDialog {
    @BindView(R.id.containerMain)
    View containerMain;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_how_can_ihelp_you, container, false);
        ButterKnife.bind((Object) this, view);
        this.containerMain.setBackground(new BitmapDrawable(getResources(), this.background));
        return view;
    }

    @OnClick({R.id.ok})
    protected void clickOk() {
        dismiss();
    }
}
