package de.seniormed24.liivie.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.seniormed24.liivie.R;

public class MeasuredValuesDialog extends BaseDialog {
    View containerMain;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_measured_values, container, false);
        v.findViewById(R.id.btOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOk();
            }
        });
        return v;
    }

    protected void clickOk() {
        dismiss();
    }
}
