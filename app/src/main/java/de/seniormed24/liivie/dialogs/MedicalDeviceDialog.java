package de.seniormed24.liivie.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.seniormed24.liivie.R;

public class MedicalDeviceDialog extends BaseDialog {
    @BindView(R.id.containerMain)
    View containerMain;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_medical_device, container, false);
        ButterKnife.bind((Object) this, view);
        //this.containerMain.setBackground(new BitmapDrawable(getResources(), this.background));
        return view;
    }

    @OnClick({R.id.ok})
    protected void clickOk() {
        String pulseStr = ((EditText) getView().findViewById(R.id.pulseEt)).getText().toString();
        String systStr = ((EditText) getView().findViewById(R.id.systEt)).getText().toString();
        String diastStr = ((EditText) getView().findViewById(R.id.diastEt)).getText().toString();
        String tempStr = ((EditText) getView().findViewById(R.id.tempEt)).getText().toString();

        if (!pulseStr.isEmpty() && Integer.valueOf(pulseStr) >= 40 && Integer.valueOf(pulseStr) <= 200 &&
                !systStr.isEmpty() && Integer.valueOf(systStr) >= 40 && Integer.valueOf(systStr) <= 250 &&
                !diastStr.isEmpty() && Integer.valueOf(diastStr) >= 40 && Integer.valueOf(diastStr) <= 190 &&
                !tempStr.isEmpty() && Integer.valueOf(tempStr) >= 35 && Integer.valueOf(tempStr) <= 37) {
            dismiss();
        }
    }
}
