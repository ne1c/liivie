package de.seniormed24.liivie.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import de.seniormed24.liivie.R;
import de.seniormed24.liivie.utils.InputFilterMinMax;

public class MinMaxDialog extends BaseDialog {

    String title;
    Listener mListener;

    int minValue;
    int maxValue;

    public MinMaxDialog() {
    }


    public static MinMaxDialog newInstance(int min, int max) {
        Bundle args = new Bundle();
        args.putInt("min", min);
        args.putInt("max", max);

        MinMaxDialog fragment = new MinMaxDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View mainView = inflater.inflate(R.layout.dialog_min_max, container, false);

        minValue = getArguments().getInt("min");
        maxValue = getArguments().getInt("max");

        mainView.findViewById(R.id.btOKMinMaxDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText min = mainView.findViewById(R.id.minEditText);
                String minText = min.getText().toString();
                EditText max = mainView.findViewById(R.id.maxEditText);
                String maxText = max.getText().toString();

                min.setFilters(new InputFilter[]{new InputFilterMinMax(minValue, maxValue)});

                if (!minText.isEmpty() && !maxText.isEmpty() &&
                        Integer.valueOf(minText) >= minValue && Integer.valueOf(maxText) <= maxValue) {
                    mListener.set(Integer.valueOf(minText), Integer.valueOf(maxText));

                    clickOk();
                }
            }
        });
        TextView tvTitleMinMax = mainView.findViewById(R.id.tvTitleMinMax);
        tvTitleMinMax.setText(title);
        return mainView;
    }

    public void setTitle(String pTitle) {
        title = pTitle;
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    protected void clickOk() {
        dismiss();
    }

    public interface Listener {
        void set(int min, int max);
    }
}
