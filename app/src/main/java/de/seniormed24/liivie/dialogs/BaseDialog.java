package de.seniormed24.liivie.dialogs;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Display;

import de.seniormed24.liivie.R;

public class BaseDialog extends DialogFragment {

    Bitmap background;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    public void onStart() {
        super.onStart();
        if (getActivity() != null) {
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            Dialog dialog = getDialog();
            if (dialog != null && dialog.getWindow() != null) {
                dialog.getWindow().setLayout(size.x, size.y + 100);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                dialog.getWindow().addFlags(Integer.MIN_VALUE);
                dialog.getWindow().getDecorView().setSystemUiVisibility(256);
                dialog.getWindow().setGravity(49);
            }
        }
    }

    public void setBackground(Bitmap background) {
        this.background = background;
    }
}
