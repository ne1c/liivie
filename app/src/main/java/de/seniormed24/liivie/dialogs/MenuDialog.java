package de.seniormed24.liivie.dialogs;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.seniormed24.liivie.R;

public class MenuDialog extends BaseDialog {
    @BindView(R.id.containerMain)
    View containerMain;
    IClickMenuListener listener;

    public interface IClickMenuListener {
        void onClick(int i);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_menu, container, false);
        ButterKnife.bind((Object) this, view);
        this.containerMain.setBackground(new BitmapDrawable(getResources(), this.background));
        return view;
    }

    @OnClick({R.id.ok})
    protected void clickOk() {
        dismiss();
    }

    @OnClick({R.id.heartRate})
    protected void clickHeartRate() {
        dismiss();
        this.listener.onClick(1);
    }

    @OnClick({R.id.temperature})
    protected void clickTemperature() {
    }

    @OnClick({R.id.bloodPressure, R.id.hrv, R.id.sweet})
    protected void clickMenus() {
    }

    public void setListener(IClickMenuListener listener) {
        this.listener = listener;
    }
}
