package de.seniormed24.liivie;

import android.app.Application;

import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;

import de.seniormed24.liivie.utils.LLog;
import de.seniormed24.liivie.utils.LocalData;

public class App extends Application {

    private LocalData localData = new LocalData();
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        LLog.i("Instabug.Builder");
        new Instabug.Builder(this, "2e37de595aea59385f98f6dc3109d99a")
                .setInvocationEvents(InstabugInvocationEvent.SHAKE)
                .build();
    }

    public static App getInstance() {
        if (instance == null) {
            synchronized (App.class) {
                if (instance == null)
                    instance = new App();
            }
        }
        // Return the instance
        return instance;
    }

    public LocalData getLocalData() {
        return localData;
    }

}
