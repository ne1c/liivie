package de.seniormed24.liivie.model;

public class Vital {

    String title;
    int value;
    String unit;
    int font;


    public Vital(String title, int value, String unit, int font) {
        this.title = title;
        this.value = value;
        this.unit = unit;
    }

    public Vital(String title, int value, String unit) {
        this(title, value, unit, 14);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
