package de.seniormed24.liivie.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferenceUtils {
    private static SharedPreferenceUtils mSharedPreferenceUtils;
    protected Context mContext;
    private SharedPreferences mSharedPreferences;
    private Editor mSharedPreferencesEditor;

    private SharedPreferenceUtils(Context context) {
        this.mContext = context;
        this.mSharedPreferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        this.mSharedPreferencesEditor = this.mSharedPreferences.edit();
    }

    public static synchronized SharedPreferenceUtils getInstance(Context context) {
        SharedPreferenceUtils sharedPreferenceUtils;
        synchronized (SharedPreferenceUtils.class) {
            if (mSharedPreferenceUtils == null) {
                mSharedPreferenceUtils = new SharedPreferenceUtils(context.getApplicationContext());
            }
            sharedPreferenceUtils = mSharedPreferenceUtils;
        }
        return sharedPreferenceUtils;
    }

    public void setValue(String key, String value) {
        this.mSharedPreferencesEditor.putString(key, value);
        this.mSharedPreferencesEditor.commit();
    }

    public void setValue(String key, int value) {
        this.mSharedPreferencesEditor.putInt(key, value);
        this.mSharedPreferencesEditor.commit();
    }

    public void setValue(String key, double value) {
        setValue(key, Double.toString(value));
    }

    public void setValue(String key, long value) {
        this.mSharedPreferencesEditor.putLong(key, value);
        this.mSharedPreferencesEditor.commit();
    }

    public void setValue(String key, boolean value) {
        this.mSharedPreferencesEditor.putBoolean(key, value);
        this.mSharedPreferencesEditor.commit();
    }

    public String getStringValue(String key, String defaultValue) {
        return this.mSharedPreferences.getString(key, defaultValue);
    }

    public int getIntValue(String key, int defaultValue) {
        return this.mSharedPreferences.getInt(key, defaultValue);
    }

    public long getLongValue(String key, long defaultValue) {
        return this.mSharedPreferences.getLong(key, defaultValue);
    }

    public boolean getBoolanValue(String keyFlag, boolean defaultValue) {
        return this.mSharedPreferences.getBoolean(keyFlag, defaultValue);
    }

    public void removeKey(String key) {
        if (this.mSharedPreferencesEditor != null) {
            this.mSharedPreferencesEditor.remove(key);
            this.mSharedPreferencesEditor.commit();
        }
    }

    public void clear() {
        this.mSharedPreferencesEditor.clear().commit();
    }
}
