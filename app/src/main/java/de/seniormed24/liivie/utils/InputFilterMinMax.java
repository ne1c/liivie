package de.seniormed24.liivie.utils;

import android.text.InputFilter;
import android.text.Spanned;

public class InputFilterMinMax implements InputFilter {
    private int max;
    private int min;

    public InputFilterMinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public InputFilterMinMax(String min, String max) {
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(dest.toString());
            stringBuilder.append(source.toString());
            if (!isInRange(this.min, this.max, Integer.parseInt(stringBuilder.toString()))) {
                return null;
            }
            return stringBuilder.toString();
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean isInRange(int r3, int r4, int r5) {
      if(r4 > r3 && r5 > r3 && r5 < r4)
          return true;
      else
          return false;

    }
}
