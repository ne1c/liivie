package de.seniormed24.liivie.utils;

import android.util.Log;

/**
 * Utility class for managing logs using single entry point.
 */
public class LLog {

    static public final String TAG = "LIIVIE";

    static public final int MASK_NONE = 0;
    static public final int MASK_VERBOSE = 1;
    static public final int MASK_DEBUG = 2;
    static public final int MASK_INFO = 4;
    static public final int MASK_WARNING = 5;
    static public final int ALL_MASK = MASK_VERBOSE | MASK_DEBUG | MASK_INFO | MASK_WARNING;

    static public final int LEVEL = ALL_MASK;

    static public final String DELIMITER = ".java :: ";

    // FIXME set ENABLED to false for release
    static public final boolean ENABLED = true;


    static public boolean isMaskEnabled(final int mask) {
        return (LEVEL & mask) > 0;
    }

    static public void d(final String msg) {
        if (ENABLED && isMaskEnabled(MASK_DEBUG)) {
            Log.d(TAG, msg);
        }
    }

    static public void d() {
        if (ENABLED && isMaskEnabled(MASK_INFO)) {
            Log.d(TAG, getStackTracePath());
        }
    }

    static public void i(final String msg) {
        if (ENABLED && isMaskEnabled(MASK_INFO)) {
            Log.i(TAG, msg + " " + getStackTracePath());
        }
    }


    static public void i() {
        if (ENABLED && isMaskEnabled(MASK_INFO)) {
            Log.i(TAG, getStackTracePath());
        }
    }


    static public void v(final String msg) {
        if (ENABLED && isMaskEnabled(MASK_VERBOSE)) {
            Log.v(TAG, msg);
        }
    }

    static public void w(final String msg) {
        if (ENABLED && isMaskEnabled(MASK_WARNING)) {
            Log.w(TAG, msg);
        }
    }

    static public void d(final Object objSource, final String msg) {
        d(objSource.getClass(), msg);
    }


    static public void d(final Class<?> clazz, final String msg) {
        d(clazz.getSimpleName() + DELIMITER + msg);
    }

/*
    static public void i(final Object objSource, final String msg) {
        i(objSource.getClass(), msg);
    }


    static public void i(final Class<?> clazz, final String msg) {
        i(clazz.getSimpleName() + DELIMITER + msg);
    }
*/

    static public void v(final Object objSource, final String msg) {
        v(objSource.getClass(), msg);
    }

    static public void v(final Class<?> clazz, final String msg) {
        v(clazz.getSimpleName() + DELIMITER + msg);
    }

    static public void w(final Object objSource, final String msg) {
        w(objSource.getClass(), msg);
    }

    static public void w(final Class<?> clazz, final String msg) {
        w(clazz.getSimpleName() + DELIMITER + msg);
    }

    static public void e(final Class<?> clazz, final String msg,
                         final Throwable e) {
        Log.e(TAG, clazz.getSimpleName() + DELIMITER + msg, e);
    }

    static public void e(final Class<?> clazz, final String msg) {
        Log.e(TAG, clazz.getSimpleName() + DELIMITER + msg);
    }

    static public void e(final Object objSource, final String msg,
                         final Throwable e) {
        e(objSource.getClass(), msg, e);
    }

    static public void e(final Object objSource, final String msg) {
        e(objSource.getClass(), msg);
    }

    static public void e(final String msg, final Throwable e) {
        Log.e(TAG, msg, e);
    }

    static public void e(final String msg) {
        Log.e(TAG, msg);
    }

    static public void e(final Throwable e) {
        e("Exception", e);
    }


    static String getStackTracePath() {

        StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
//        d(stackTraceElement.toString());
        int currentIndex = -1;
        boolean firstFound = false;
        for (int i = 0; i < stackTraceElement.length; i++) {
            if (stackTraceElement[i].getFileName().compareTo("LLog.java") == 0) {
                firstFound = true;
            } else {
                if (firstFound) {
                    currentIndex = i;
                    break;
                }
            }
        }

        String fileName = stackTraceElement[currentIndex].getFileName();
        String methodName = stackTraceElement[currentIndex].getMethodName();
        String lineNumber = String.valueOf(stackTraceElement[currentIndex].getLineNumber());

        return "@" + methodName + "() (" + fileName + ":" + lineNumber + ")";

    }

}