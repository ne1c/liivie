package de.seniormed24.liivie.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Pair;

public class SharePref {

    public static final String PREFERENCE_KEY_FTU_FINISHED = "PREFERENCE_KEY_FTU_FINISHED";

    private static SharePref sInstance = null;

    private boolean isFtuFinished;

    private SharedPreferences localPref;

    private SharePref(Context ctx) {
        localPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        isFtuFinished = localPref.getBoolean(PREFERENCE_KEY_FTU_FINISHED, false);
    }

    public static SharePref i(Context ctx) {
        if (sInstance == null) {
            sInstance = new SharePref(ctx);
        }
        return sInstance;
    }

    public boolean isFtuFinished() {
        isFtuFinished = localPref.getBoolean(PREFERENCE_KEY_FTU_FINISHED, false);
        return isFtuFinished;
    }

    public void setFtuFinished(boolean isFtuFinished) {
        if (isFtuFinished != isFtuFinished()) {
            localPref.edit().putBoolean(PREFERENCE_KEY_FTU_FINISHED, isFtuFinished).apply();
        }
    }

    public void setBloodPressure(int min, int max) {
        localPref.edit().putInt("bp_min", min).putInt("bp_max", max).apply();
    }

    public Pair<Integer, Integer> getBloodPressure() {
        return new Pair<>(localPref.getInt("bp_min", 0), localPref.getInt("bp_max", 0));
    }

    public void setTemp(int min, int max) {
        localPref.edit().putInt("temp_min", min).putInt("temp_max", max).apply();
    }

    public Pair<Integer, Integer> getTemp() {
        return new Pair<>(localPref.getInt("temp_min", 0), localPref.getInt("temp_max", 0));
    }

    public void setHeartRate(int min, int max) {
        localPref.edit().putInt("hr_min", min).putInt("hr_max", max).apply();
    }

    public Pair<Integer, Integer> getHeartRate() {
        return new Pair<>(localPref.getInt("hr_min", 0), localPref.getInt("hr_max", 0));
    }
}
