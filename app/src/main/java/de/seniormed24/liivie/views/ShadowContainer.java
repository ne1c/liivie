package de.seniormed24.liivie.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import de.seniormed24.liivie.R;

public class ShadowContainer extends RelativeLayout {

    private static final int SHADOW_SIZE = 18;

    private LinearLayout container;
    private float shadowAlpha = 0.25f;
    private int shadowImage = R.drawable.shadow;
    private int shadowSize = SHADOW_SIZE;

    public ShadowContainer(Context context) {
        super(context);
        init(context);
    }

    public ShadowContainer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, new int[]{R.attr.shadow_alpha, R.attr.shadow_image, R.attr.shadow_size}, 0, 0);
        try {
            this.shadowImage = ta.getResourceId(1, R.drawable.shadow);
            this.shadowAlpha = ta.getFloat(0, 0.25f);
            this.shadowSize = ta.getInteger(2, SHADOW_SIZE);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ShadowContainer: ");
            stringBuilder.append(this.shadowImage);
            Log.w("myLog", stringBuilder.toString());
            init(context);
        } finally {
            ta.recycle();
        }
    }

    public ShadowContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.obtainStyledAttributes(attrs, new int[]{R.attr.shadow_alpha, R.attr.shadow_image, R.attr.shadow_size}, 0, 0);
        try {
            this.shadowImage = ta.getResourceId(1, R.drawable.shadow);
            this.shadowAlpha = ta.getFloat(0, 0.25f);
            this.shadowSize = ta.getInteger(2, SHADOW_SIZE);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ShadowContainer: ");
            stringBuilder.append(this.shadowImage);
            Log.w("myLog", stringBuilder.toString());
            init(context);
        } finally {
            ta.recycle();
        }
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            View view = inflater.inflate(R.layout.view_shadow_container, this, true);
            this.container = view.findViewById(R.id.fragmentContainer);
            LinearLayout paddingContainer = (LinearLayout) view.findViewById(R.id.paddingContainer);
            ImageView shadow = (ImageView) view.findViewById(R.id.shadow);
            shadow.setImageResource(this.shadowImage);
            shadow.setAlpha(this.shadowAlpha);
            paddingContainer.setPadding(0, 0, 0, (int) (((float) this.shadowSize) * getResources().getDisplayMetrics().density));
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
       someMethod();
    }

    public void someMethod() {
        while (true) {
            View childAt = getChildAt(1);
            View v = childAt;
            if (childAt != null) {
                removeView(v);
                this.container.addView(v);
            } else {
                return;
            }
        }
    }
}
