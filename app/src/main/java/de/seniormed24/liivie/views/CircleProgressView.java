package de.seniormed24.liivie.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import de.seniormed24.liivie.R;

public class CircleProgressView extends RelativeLayout {
    CircleProgress circleProgress;
    float currentProgress = 0.0f;
    int padding = 20;
    private boolean showDouble = false;
    AppCompatTextView textView;
    public static final int[] CircleProgressView = new int[]{R.attr.back_color, R.attr.current_progress, R.attr.empty_progress_line_color, R.attr.max_progress, R.attr.progress_line_color, R.attr.show_double, R.attr.text_padding};


    private class CircleProgress extends View {
        private int backgroundColor;
        private double currentProgress;
        private int emptyProgressLineColor;
        private double mThickness;
        private double mThicknessAround;
        private double maxProgress;
        private int progressLineColor;

        public CircleProgress(CircleProgressView circleProgressView, Context context) {
            this(circleProgressView, context, null);
        }

        public CircleProgress(CircleProgressView circleProgressView, Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        public CircleProgress(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            this.mThickness = 0.06d;
            this.mThicknessAround = 0.03d;
            this.maxProgress = 100.0d;
            this.currentProgress = 35.5d;
            this.backgroundColor = -1;
            this.emptyProgressLineColor = -7829368;
            this.progressLineColor = -16777216;
        }

        public void setEmptyProgressLineColor(int emptyProgressLineColor) {
            this.emptyProgressLineColor = emptyProgressLineColor;
        }

        public void setMaxProgress(double maxProgress) {
            this.maxProgress = maxProgress;
        }

        public void setProgressLineColor(int progressLineColor) {
            this.progressLineColor = progressLineColor;
        }

        public void invalidate() {
            super.invalidate();
        }

        protected void onDraw(Canvas canvas) {
            int width = getWidth();
            int height = getHeight();
            Paint AroundCirclePaint = new Paint();
            AroundCirclePaint.setColor(-1);
            RectF AroundCircle = new RectF();
            AroundCircle.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
            canvas.drawArc(AroundCircle, 0.0f, 360.0f, true, AroundCirclePaint);
            Paint bigCirclePaint = new Paint();
            bigCirclePaint.setColor(this.emptyProgressLineColor);
            RectF bigCircle = new RectF();
            bigCircle.set((float) ((int) (((double) height) * this.mThicknessAround)), (float) ((int) (((double) height) * this.mThicknessAround)), (float) (getWidth() - ((int) (((double) height) * this.mThicknessAround))), (float) (getHeight() - ((int) (((double) height) * this.mThicknessAround))));
            canvas.drawArc(bigCircle, 0.0f, 360.0f, true, bigCirclePaint);
            Paint progressCirclePaint = new Paint();
            progressCirclePaint.setColor(this.progressLineColor);
            canvas.drawArc(bigCircle, 270.0f, (float) ((int) ((this.currentProgress / this.maxProgress) * 360.0d)), true, progressCirclePaint);
            Paint smallCirclePaint = new Paint();
            smallCirclePaint.setColor(this.backgroundColor);
            RectF smallCircle = new RectF();
            smallCircle.set((float) ((int) ((((double) width) * this.mThickness) * 2.0d)), (float) ((int) ((((double) height) * this.mThickness) * 2.0d)), (float) (width - ((int) ((((double) width) * this.mThickness) * 2.0d))), (float) (height - ((int) ((((double) height) * this.mThickness) * 2.0d))));
            canvas.drawArc(smallCircle, 0.0f, 360.0f, true, smallCirclePaint);
        }

        public void setCurrentProgress(double currentProgress) {
            this.currentProgress = currentProgress;
            invalidate();
        }

        public void setBackgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
        }
    }

    public CircleProgressView(Context context) {
        super(context);
        init();
    }

    public CircleProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        setAttr(attrs);
    }

    public CircleProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        setAttr(attrs);
    }

    public void setAttr(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, new int[]{R.attr.back_color, R.attr.current_progress, R.attr.empty_progress_line_color, R.attr.max_progress, R.attr.progress_line_color, R.attr.show_double, R.attr.text_padding}, 0, 0);
        try {
            this.circleProgress.setBackgroundColor(ta.getColor(0, -1));
            this.circleProgress.setEmptyProgressLineColor(ta.getColor(2, getResources().getColor(R.color.gray)));
            this.circleProgress.setProgressLineColor(ta.getColor(4, -16777216));
            this.circleProgress.setMaxProgress((double) ta.getFloat(3, 100.0f));
            this.circleProgress.setCurrentProgress((double) ta.getFloat(1, 67.0f));
            this.currentProgress = ta.getFloat(1, 67.0f);
            this.showDouble = ta.getBoolean(5, false);
            this.padding = ta.getInteger(6, 20);
            resizeTextView();
        } finally {
            ta.recycle();
        }
    }

    protected void init() {
        this.circleProgress = new CircleProgress(this, getContext());
        addView(this.circleProgress);
        this.textView = new AppCompatTextView(getContext());
        this.textView.setLayoutParams(new LayoutParams(-1, -1));
        TextViewCompat.setAutoSizeTextTypeWithDefaults(this.textView, 1);
        this.textView.setText(this.showDouble ? String.valueOf(this.currentProgress) : String.valueOf((int) this.currentProgress));
        this.textView.setGravity(17);
        this.textView.setTextColor(-16777216);
        this.textView.setTypeface(Typeface.create("sans-serif-black", Typeface.NORMAL));
        this.textView.setBackgroundResource(R.color.transparent);
        addView(this.textView);
    }

    public void setCurrentProgress(float currentProgress) {
        this.currentProgress = currentProgress;
        this.textView.setText(this.showDouble ? String.valueOf(currentProgress) : String.valueOf((int) currentProgress));
    }

    void resizeTextView() {
        this.textView.setPadding((int) (((float) this.padding) * getResources().getDisplayMetrics().density), (int) (((float) this.padding) * getResources().getDisplayMetrics().density), (int) (((float) this.padding) * getResources().getDisplayMetrics().density), (int) (((float) this.padding) * getResources().getDisplayMetrics().density));
        setCurrentProgress(this.currentProgress);
    }
}
