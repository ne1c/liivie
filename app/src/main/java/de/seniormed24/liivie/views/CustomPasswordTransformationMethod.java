package de.seniormed24.liivie.views;

import android.text.method.PasswordTransformationMethod;
import android.view.View;

public class CustomPasswordTransformationMethod extends PasswordTransformationMethod {

    private class PasswordCharSequence implements CharSequence {
        private CharSequence mSource;

        PasswordCharSequence(CharSequence source) {
            this.mSource = source;
        }

        public char charAt(int index) {
            return '*';
        }

        public int length() {
            return this.mSource.length();
        }

        public CharSequence subSequence(int start, int end) {
            return this.mSource.subSequence(start, end);
        }
    }

    public CharSequence getTransformation(CharSequence source, View view) {
        return new PasswordCharSequence(source);
    }
}
